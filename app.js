angular.module('myApp', ['ngRoute']).config(config);

function config($routeProvider) {
  $routeProvider.when('/', {
    templateUrl: "main/main.html",
    controller: 'MainController',
    controllerAs: 'vm'
  }).when('/about',{
    templateUrl: "about/about.html",
    controller: 'AboutController',
    controllerAs: 'vm'
  });
}

